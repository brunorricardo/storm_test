## Teste tecnico - StormTech

Aqui estão os arquivos fontes do teste (*./src*) e também o dockerfile
para rodar o teste.

##### Para rodar a aplicação, siga os passos:

1. Instale o [Docker](https://www.docker.com/community-edition "Docker") e o [Docker Compose](https://docs.docker.com/compose/install/ "Docker Compose")
2. Faça o clone deste repositório
3. Dentro do diretorio clonado, rode o comando `docker-compose build` (*Caso linux, deve estar acompanhado de* **sudo**)
4. Assim que o build da imagem do LAMP for concluido, execute a imagem com o comando: `docker-compose up`
5. A aplicação vai ser iniciada em:  **localhost:8081**