let tabledata = [];

let init = () => {
    createData();
    // populateTable(tabledata);
    tableSort();
};

let createData = () => {
    tabledata.push({'num': 1, title: 'Java How to Program', author: 'Deitel & Deitel', editionYear: 2007});

    tabledata.push({
        'num': 4,
        title: 'Internet & World Wide Web: How to Program',
        author: 'Deitel & Deitel',
        editionYear: 2007
    });
    tabledata.push({
        'num': 2,
        title: 'Patterns of Enterprise Application Architecture',
        author: 'Martin Fowler',
        editionYear: 2002
    });

    tabledata.push({'num': 3, title: 'Head First Design Patterns', author: 'Elisabeth Freeman', editionYear: 2004});
}

let populateTable = (data) => {

    $('#booksTable > tbody').html('');

    data.forEach((book) => {
        let row = `<tr>
                               <td>${book.num}</td>
                               <td>${book.title}</td>
                               <td>${book.author}</td>
                               <td>${book.editionYear}</td>
                           </tr>`;

        $('#booksTable > tbody').append(row);
    })
}

let getFormVals = () => {

    let fields = [];
    let fieldsDir = [];

    fields = $.map($('select[name="field"]'), function (val, _) {
        return val.value;
    });

    fields = fields.filter(function (e) {
        return e
    });

    fieldsDir = $.map($('select[name="fieldDir"]'), function (val, _) {
        return val.value;
    });

    fieldsDir = fieldsDir.filter(function (e) {
        return e
    });

    return {fields, fieldsDir};
};

let normalizeSorters = (composition) => {

    let filtros = [];

    for (let i = 0; i < composition.fields.length; i++){
        filtros.push([composition.fields[i], parseInt(composition.fieldsDir[i])]);
    }

    return filtros;
}

let tableSort = () => {

    let filtros = normalizeSorters(getFormVals());

    $.ajax({
        url: 'back-end/books.php',
        type: 'GET',
        data: {
            books: JSON.stringify(tabledata),
            filtros: JSON.stringify(filtros)
        },
        dataType: 'JSON',
        success: (res) => {
            populateTable(res);
        }
    })

}