<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once 'classes/SortingServiceException.php';
require_once 'classes/Sorter.php';

$reqParams = $_GET;
$books;

try{
    $books = json_decode($reqParams['books'], true);
}catch(SortingServiceException $sse){
    throw new SortingServiceException('The Books array must be non-null');
}

$filtros = json_decode($reqParams['filtros']);

$sortedBooks = usort($books, Sorter::array_multi_sorter($filtros));
echo json_encode($books, JSON_NUMERIC_CHECK);


