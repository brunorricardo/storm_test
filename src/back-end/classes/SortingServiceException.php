<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17/07/18
 * Time: 21:49
 */

class SortingServiceException extends Exception
{

    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}