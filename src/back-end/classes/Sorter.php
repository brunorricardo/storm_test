<?php

class Sorter
{

    public static function array_multi_sorter($criteria)
    {

        foreach ($criteria as $index => $criterion) {
            $criteria[$index] = is_array($criterion) ? array_pad($criterion, 3, null) : array(
                $criterion,
                SORT_ASC,
                null
            );
        }

        return function ($first, $second) use (&$criteria) {
            foreach ($criteria as $criterion) {

                list($column, $sortOrder, $projection) = $criterion;
                $sortOrder = $sortOrder === SORT_DESC ? -1 : 1;

                if ($projection) {
                    $lhs = call_user_func($projection, $first[$column]);
                    $rhs = call_user_func($projection, $second[$column]);
                } else {
                    $lhs = $first[$column];
                    $rhs = $second[$column];
                }

                if ($lhs < $rhs) {
                    return -1 * $sortOrder;
                } else {
                    if ($lhs > $rhs) {
                        return 1 * $sortOrder;
                    }
                }
            }

            return 0;
        };
    }
}